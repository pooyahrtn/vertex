/** @format */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

String.prototype.toPersian = function () {
    var id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    
    return this.replace('.', ',').replace(/[0-9]/g, function (w) {
        return id[+w]
    });
}

AppRegistry.registerComponent(appName, () => App);
