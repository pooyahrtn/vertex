import React from 'react';
import Menu, { MenuButton } from './src/screens/Home/Menu';
import { Animated, PanResponder, View, Dimensions, TouchableOpacity, Text, SafeAreaView, Easing } from 'react-native';
import { Icon } from 'react-native-elements';
const { width } = Dimensions.get('screen');

const buttons: [MenuButton] = [
  {
    height: 120,
    degree: 20,
    radius: 30
  },
  {
    height: 150,
    degree: 110,
    radius: 35
  },
  {
    height: 130,
    degree: 190,
    radius: 26
  },
  {
    height: 150,
    degree: 300,
    radius: 30
  },
  {
    height: 110,
    degree: 250,
    radius: 25
  }
]

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu_expanded: false,

    }

    this.animatedValue = new Animated.ValueXY({ x: 0, y: width });
    this._value = { x: 0, y: width }
    this.animation = this.animatedValue.y.interpolate({ inputRange: [0, width], outputRange: [1, 0] })

    this.animatedValue.addListener((value) => {
      this._value = value;
      // this.animation.setValue((width - value.y) / width);
    });
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderGrant: (e, gestureState) => {
        this.animatedValue.setOffset({
          x: 0,
          y: this._value.y,
        })
        this.animatedValue.setValue({ x: 0, y: 0 })
      },
      onPanResponderMove: Animated.event([
        null, { dy: this.animatedValue.y }
      ]),
      onPanResponderRelease: (e, gestureState) => {
        this.animatedValue.flattenOffset();

        if (gestureState.vy < -0.3) {
          this.expandMenu();
        }
        else if (gestureState.vy > 0.3) {
          this.closeMenu();
        }
        else if (this._value.y > width / 1.5 && !this.state.menu_expanded) {
          this.closeMenu();
        }
        else if (this._value.y < width / 1.5 && this.state.menu_expanded) {
          this.expandMenu();
        } else {
          this.toggleMenu();
        }

      },
    })

  }
  expandMenu = () => {
    Animated.spring(this.animatedValue, { toValue: { y: 0, x: 0 } }).start();
    this.setState({
      menu_expanded: true
    });
  }
  closeMenu = () => {
    Animated.timing(this.animatedValue, { toValue: { y: width, x: 0 }, duration: 300 }).start();
    this.setState({
      menu_expanded: false
    });
  }

  toggleMenu = () => {
    if (this.state.menu_expanded) {
      this.closeMenu();
    } else {
      this.expandMenu();
    }
  }

  render() {
    let menu_y_translation = this.animatedValue.y.interpolate({
      inputRange: [-Infinity, 0, width, Infinity],
      outputRange: [0, 0, width, width]
    })
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Animated.View

            style={{ bottom: 0, position: 'absolute', transform: [{ translateY: menu_y_translation }] }}
            {...this.panResponder.panHandlers}
          >
            <TouchableOpacity
              activeOpacity={1}
              onPress={this.toggleMenu}
              style={{
                backgroundColor: 'black',
                alignItems: 'center',
                paddingBottom: 6,
                paddingTop: 6
              }}
            >
              {
                this.state.menu_expanded ?
                  <Icon name='expand-more' color='white' size={45} />
                  :
                  <Icon name='expand-less' color='white' size={45} />
              }

            </TouchableOpacity>

            <Menu animation={this.animation} buttons={buttons} />
          </Animated.View>
        </View>


      </SafeAreaView>
    );
  }
}