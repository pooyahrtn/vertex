// @flow
import Router from '../router';
import React from 'react';
import { Provider } from 'unstated';

const App = () => {
  return (
    <Provider>
      <Router />
    </Provider>
  )
}

export default App

