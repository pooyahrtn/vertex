export default class AnimationQueue {
    runningAnimation = false;
    queue: { animation: any, onEnd: () => void }[] = [];

    run = ({ animation, onEnd }) => {
        this.queue.push({ animation, onEnd });
        if (!this.runningAnimation) {
            this.runningAnimation = true;
            animation.start(() => this._onEnd(onEnd));
        }
    }
    _onEnd = (onEnd) => {
        this.runningAnimation = false;
        if (onEnd) { onEnd() }
        this.queue = this.queue.slice(1);
        this._run_next();
    }
    _run_next = () => {
        if (this.queue.length > 1) {
            const animation = this.queue[1];
            this.run(animation);
        }
    }
}