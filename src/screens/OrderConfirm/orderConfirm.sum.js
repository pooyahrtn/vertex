import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from '../../components';
import Dash from 'react-native-dash';

type Props = {
    value: number
}
export default function (props: Props) {
    return (
        <View style={styles.container}>
            <Dash style={{ width: '100%', height: 1 }} dashGap={10} dashThickness={1} dashLength={6} />
            <View style={styles.rowContainer}>
                <Text style={styles.sumText}>Σ = {String(props.value).toPersian()}</Text>
                <Text >یون</Text>
            </View>
            <View style={styles.rowContainer}>
                <Text style={styles.sumText}>Δi = {String(props.value).toPersian()}</Text>
                <Text >یون</Text>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingBottom: 100,
        paddingHorizontal: 10,
    },
    sumText: {
        fontSize: 35, paddingVertical: 10,
        textAlign: 'left', flex: 1,
        direction: 'ltr',
    },
    rowContainer: { flexDirection: 'row', alignItems: 'center' },

});