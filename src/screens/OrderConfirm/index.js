import OrderDetailScreen from './orderConfirm.screen';
import { OrderContainer } from '../../containers/Order';
import connectToContainer from '../../containers/utils/containerWrapper';

export default connectToContainer(OrderDetailScreen, {}, OrderContainer);