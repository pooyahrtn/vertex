import React from 'react';
import { } from '../../components';
import { SafeAreaView, FlatList } from 'react-native';
import styles from './orderConfirm.styles';
import { OrderContainer } from '../../containers/Order';
import OrderItem from '../Order/order.item';
import Footer from './orderConfirm.sum';

type Props = {
    container: OrderContainer
}

export default class OrderConfirmScreen extends React.Component<Props> {
    render() {
        return (
            <SafeAreaView style={styles.screen}>
                <FlatList
                    data={this.props.container.state.orders}
                    extraData={this.props.container.state}
                    keyExtractor={(item) => item.item.id}
                    renderItem={({ item }) => <OrderItem
                        item={item.item}
                        count={item.count}
                        onAddPressed={() => this.props.container.addOrderItem(item.item)}
                        onSubtract={() => this.props.container.subtractItem(item.item.id)}
                    />}
                    ListFooterComponent={<Footer value={this.props.container.getTotalPriceSum()}/>}
                />
            </SafeAreaView>
        );
    }
}