import React from 'react';
import {
    View, StyleSheet, Platform,
    TouchableWithoutFeedback, Keyboard, Dimensions, KeyboardAvoidingView
} from 'react-native';
import { VertexCircle, Text } from '../../components';
import { View as AnimatableView } from 'react-native-animatable';
import FormInput from './FormInput';
import { SafeAreaView } from 'react-navigation';
// import gql from "graphql-tag";
// import { Mutation } from "react-apollo";


const screenHeight = Dimensions.get('screen').height;
const CIRCLE_TRANSITION = 160;

type State = {
    state: 'intro' | 'enter_phone' | 'login' | 'sign_up',
    loading: boolean
}

const STATES = {
    intro: 'intro',
    enter_phone: 'enter_phone',
    login: 'login',
    sign_up: 'sign_up'
};

const SHOW_LOADING = false;

export default class SingUpContainer extends React.Component {
    state: State = {
        state: STATES.intro,
        loading: false,

    }
    constructor(props) {
        super(props);

    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    _keyboardDidShow = (e) => {
        const keyboardHeight = e.endCoordinates.height;
        this.transitCircle((screenHeight / 2) - keyboardHeight - 40);
    }
    _keyboardDidHide = (e) => {
        const keyboardHeight = e.endCoordinates.height;
        this.transitCircle(CIRCLE_TRANSITION);
    }

    onIntroEnd = () => {

        this.setState({
            state: STATES.enter_phone
        });

    }
    shouldLogin = () => {
        this.props.submit();
        this.setState({
            loading: true
        });
        setTimeout(() => {
            if (SHOW_LOADING) {
                this.setState({
                    loading: false,
                    state: STATES.login
                });

            } else {
                this.setState({
                    loading: false,
                    state: STATES.sign_up
                });

            }

        }, 3000);
    }
    formTitle = () => {
        switch (this.state.state) {
            case STATES.enter_phone:
                return 'ثبت نام / ورود '
            case STATES.login:
                return 'ورود'
            default:
                return 'ثبت نام'

        }
    }

    render() {

        return (
            <TouchableWithoutFeedback onPress={() => {
                Keyboard.dismiss();
            }}>

                <SafeAreaView style={styles.container}>

                    <KeyboardAvoidingView behavior={Platform.OS === 'ios' && 'position'} keyboardVerticalOffset={80}>
                        {
                            shouldShow(this.state.state, [STATES.sign_up, STATES.login, STATES.enter_phone]) &&
                            <AnimatableView style={{ marginTop: 20 }} animation='fadeIn' delay={200}>
                                <Text style={{ textAlign: 'center', fontSize: 20, paddingBottom: 60 }}>{this.formTitle()}</Text>
                            </AnimatableView>
                        }

                        {
                            shouldShow(this.state.state, [STATES.login, STATES.sign_up, STATES.enter_phone]) &&
                            <AnimatableView animation='fadeIn' delay={300}>
                                <FormInput title='شماره تماس' autoFocus textInputProps={{ keyboardType: 'phone-pad' }} />
                            </AnimatableView>
                        }

                        {
                            shouldShow(this.state.state, [STATES.login, STATES.sign_up]) &&
                            <AnimatableView animation='fadeIn' delay={500}>
                                <FormInput autoFocus title='رمز' />
                            </AnimatableView>
                        }
                        {
                            shouldShow(this.state.state, [STATES.sign_up]) &&
                            <AnimatableView animation='fadeIn' delay={550}>
                                <FormInput title='تکرار رمز' textInputProps={{ keyboardType: 'phone-pad' }} />
                            </AnimatableView>
                        }
                        {
                            shouldShow(this.state.state, [STATES.sign_up]) &&
                            <AnimatableView animation='fadeIn' delay={600}>
                                <FormInput title='کد فعالسازی' textInputProps={{ keyboardType: 'phone-pad' }} />
                            </AnimatableView>
                        }

                    </KeyboardAvoidingView>

                    <Text style={{ textAlign: 'center', fontSize: 27, position: 'absolute', bottom: 100, width: '100%' }}>VERTEX</Text>


                    <VertexCircle
                        loading={this.state.loading}
                        translateY={CIRCLE_TRANSITION}
                        onAnimationEnded={this.onIntroEnd}
                        onPress={() => { this.shouldLogin() }}
                        startOnRender={true} secondRadius={30} delay={500}
                        setTransitionRef={(ref) => this.transitCircle = ref}
                    />

                </SafeAreaView>

            </TouchableWithoutFeedback>

        );
    }
}

/**
 * @param {object} state component state
 * @param {string} conditions list of conditions, if any of conditions are true, show the component
 */
function shouldShow(state, conditions: string[]) {
    let shouldReturn = false;
    conditions.forEach(condition => shouldReturn = shouldReturn || (state === condition));
    return shouldReturn;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});


// const SignUP = gql`
//   mutation {
//     signUp(input: $input) {
//         msg
//         success
//     }
//   }
// `;

// const SignUP = gql`
//   mutation signUp($input: UserAuth!) {
//     signUp(input: $input) {
//       msg
//       success
//     }
//   }
// `;
// export default () => {
//     return (
//         <Mutation mutation={SignUP}>
//             {(signUp, { data, loading, error }) => {

//                 const submit = () => signUp({ variables: { input: { username: 'pooffya', password: '1232423213' } } });

//                 console.warn(error);
//                 console.warn({ data })
//                 return <SingUpContainer submit={submit} />;
//             }
//             }
//         </Mutation>
//     );
// }


