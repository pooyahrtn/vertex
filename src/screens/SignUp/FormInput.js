import React from 'react';
import { View, TextInput, StyleSheet, TextInputProps } from 'react-native';
import { Text } from '../../components';


type Props = {
    title: string,
    textInputProps: TextInputProps,
    setInputRef: (ref: any) => void,
    autoFocus: boolean,
    password: boolean
}
export default function FormInput(props: Props) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{[props.title]}</Text>
            <TextInput
                autoFocus={props.autoFocus}
                style={[styles.textInput]}
                ref={props.setInputRef}
                secureTextEntry={props.password}
                {...props.textInputProps}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        borderColor: 'black',
        borderRightWidth: 0.5,
        borderBottomWidth: 0.5,
        marginLeft: 40,
        marginRight: 40,
        marginBottom: 15,
        alignItems: 'flex-end'
    },
    title: {
        paddingLeft: 5,

    },
    textInput: {
        width: '100%',
        height: 40,
        paddingLeft: 5
    }
});