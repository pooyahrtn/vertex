/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    View, Dimensions, Animated,
    TouchableOpacity, ViewStyle,
    StyleSheet, Text
} from 'react-native';
import { Svg, Path } from 'react-native-svg';
import { Line, AUX } from '../../components';
import Image from 'react-native-fast-image';
const { width, height } = Dimensions.get('window');
import { Icon } from 'react-native-elements';

const X_CENTER = width / 2;
const Y_CENTER = height / 2;

function degreeToRadian(degree) {
    return (2 * Math.PI * degree) / 360;
}

function locationInterpolator(animation: Animated.Value, height, degree, offset = 0) {
    let x = animation.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [
            X_CENTER - offset,
            X_CENTER + Math.cos(degreeToRadian(degree - 30)) * height / 2 - offset,
            X_CENTER + Math.cos(degreeToRadian(degree)) * height - offset
        ]
    });
    let y = animation.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [
            Y_CENTER - offset,
            Y_CENTER - Math.sin(degreeToRadian(degree - 30)) * height / 2 - offset,
            Y_CENTER - Math.sin(degreeToRadian(degree)) * height - offset
        ]
    });

    return { x, y };
}

function createTranslate(animation: Animated.Value, height, degree) {
    let x = animation.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, + Math.cos(degreeToRadian(degree - 30)) * height / 2, Math.cos(degreeToRadian(degree)) * height]
    });
    let y = animation.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, - Math.sin(degreeToRadian(degree - 30)) * height / 2, - Math.sin(degreeToRadian(degree)) * height]
    });
    return [{
        translateY: y,
    }, {
        translateX: x
    }]
}

export type MenuButton = {
    radius: number,
    height: number,
    icon: string,
    text: string,
    onPress: () => void
}

export type Props = {
    animation: Animatable.Value,
    buttons: [
        MenuButton
    ],
    containerStyle: ViewStyle,
    navigation: any,
};
export default class App extends React.PureComponent<Props> {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <View
                style={[this.props.containerStyle, { width, height }]}
            >
                <Svg
                    height={height}
                    width={width}
                >

                    {
                        this.props.buttons.map((item, index) =>

                            <Line
                                key={String(index)}
                                x1={X_CENTER}
                                y1={Y_CENTER}
                                x2={locationInterpolator(this.props.animation, item.height, item.degree).x}
                                y2={locationInterpolator(this.props.animation, item.height, item.degree).y}
                                stroke="black"
                                strokeWidth="3"
                            />


                        )
                    }


                </Svg>
                {
                    this.props.buttons.map((item, index) => (
                        <Animated.View
                            key={String(index)}
                            style={[styles.buttonAnimatedContainer, {
                                top: Y_CENTER - item.radius,
                                left: X_CENTER - item.radius,
                                transform: createTranslate(this.props.animation, item.height, item.degree),
                                width: 2 * item.radius,
                                height: 2 * item.radius,
                                borderRadius: item.radius,

                            }]}
                        >
                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate(item.screenName)}>
                                {item.icon &&
                                    <Icon name={item.icon} color='white' size={40} />
                                }
                                {
                                    item.text &&
                                    <Text style={{
                                        color: 'white',
                                        fontWeight: 'bold', fontSize: 30, fontFamily: 'Arial'
                                    }}>{item.text}</Text>
                                }
                            </TouchableOpacity>

                        </Animated.View>
                    )
                    )}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    buttonAnimatedContainer: {
        position: 'absolute',
        backgroundColor: 'black',
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    }
})