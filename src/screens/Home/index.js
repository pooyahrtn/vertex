import React from 'react';
import { View, Image, Animated, StyleSheet, Dimensions, Easing } from 'react-native';
import { Svg, Path } from 'react-native-svg';
import ProgressCircle from '../../components/ProgressCircle';
import Menu from './Menu';
import { SafeAreaView } from 'react-navigation';
import { screens } from '../../router/names';

type Props = {

}
export default class Container extends React.Component<>{
    constructor(props) {
        super(props);
        this.animated_value = new Animated.Value(1);
        this.circle_animation_value = new Animated.Value(0);
    }

    componentDidMount() {


    }

    startAnimations = () => {
        Animated.sequence([
            Animated.timing(this.animated_value, {
                toValue: 0,
                duration: 1000,
            }),
            Animated.spring(this.circle_animation_value, {
                toValue: 1,
                useNativeDriver: true,

                tension: 20
            })
        ]).start();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Menu
                    containerStyle={{ position: 'absolute' }}
                    buttons={buttons}
                    animation={this.circle_animation_value}
                    navigation={this.props.navigation}
                />
                <View style={{ width: CIRCLE_HEIGHT + 2 * CIRCLE_MARGTIN }}>

                    <Image style={styles.imageContainer}
                        onLoad={() => this.startAnimations()}
                        source={{ uri: 'https://i.pinimg.com/736x/86/29/b6/8629b6190887522dd9676c7cc4296395--black-white-fashion-fashion-portraits.jpg' }} />

                    <Animated.View style={[styles.imageContainer, { position: 'absolute', backgroundColor: 'black', opacity: this.animated_value }]} />

                    <ProgressCircle
                        containerStyle={{ position: 'absolute' }}
                        width={CIRCLE_HEIGHT + 2 * CIRCLE_MARGTIN}
                        padding={6}
                        animationProgress={this.animated_value.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 0]
                        })}
                        value={0.3}
                    />

                </View>

            </View >
        );
    }
}

const CIRCLE_HEIGHT = 100;
const CIRCLE_MARGTIN = 8;

const styles = StyleSheet.create({
    imageContainer: {
        left: CIRCLE_MARGTIN, top: CIRCLE_MARGTIN,
        height: CIRCLE_HEIGHT, width: CIRCLE_HEIGHT, borderRadius: CIRCLE_HEIGHT / 2
    }
});

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;
const _maxLineLength = (angle) => {
    const _degree = ((angle + 0.1) * Math.PI) / 180;

    const widthLimit = (screenWidth) / (Math.abs(Math.cos(_degree)) * 2) - 65;
    const heightLimit = (screenHeight) / (Math.abs(Math.sin(_degree)) * 2) - 255;

    return Math.min(widthLimit, heightLimit);
}

const _itemLineHeight = (angle) => {
    const random = 1 - (Math.random() / 10);
    return _maxLineLength(angle) * random;
}

const itemPosition = (index, n) => {
    const angle = (360 / n) * index;
    const angle_free_space = 10;
    const angle_random_c = (0.5 - Math.random()) * angle_free_space;
    const _angle = angle + angle_random_c;
    return {
        angle: _angle,
        height: _itemLineHeight(_angle)
    }
}

export type MenuButton = {
    radius: number,
    height: number,
    icon: string,
    screenName: string
}

const n = 4;
const buttons: [MenuButton] = [
    {
        height: itemPosition(0, n).height,
        degree: itemPosition(0, n).angle,
        radius: 36,
        icon: 'schedule'
    },
    {
        height: itemPosition(1, n).height,
        degree: itemPosition(1, n).angle,
        radius: 36,
        icon: 'search'
    },
    {
        height: itemPosition(2, n).height,
        degree: itemPosition(2, n).angle,
        radius: 36,
        icon: 'settings'
    },
    {
        height: itemPosition(3, n).height,
        degree: itemPosition(3, n).angle,
        radius: 36,
        icon: 'add',
        screenName: screens.order
    },

]