import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('screen').width;
const confirmButtonMaxSize = 40;

export default StyleSheet.create({
    confirmButton: {
        position: 'absolute',
        bottom: 45,
        left: (screenWidth - confirmButtonMaxSize) / 2,
    },
    confirmButtonAnimatedView: {
        backgroundColor: 'black',
        width: confirmButtonMaxSize,
        height: confirmButtonMaxSize,
        borderRadius: confirmButtonMaxSize / 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    screen: {
        flex: 1
    }
});