import OrderScreen from './order.screen';
import { OrderContainer } from '../../containers/Order';
import connectToContainer from '../../containers/utils/containerWrapper';

export default connectToContainer(OrderScreen, {}, OrderContainer);