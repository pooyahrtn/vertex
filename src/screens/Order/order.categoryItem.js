import React from 'react';
import { StyleSheet, View } from 'react-native';
import Image from 'react-native-fast-image';
import { SpringButton } from '../../components';

type Props = {
    item: any,
    selectedCategory: String,
    onSelect: () => void,
    index: number
}
export default (props: Props) => (
    <View style={styles.container}>
        <SpringButton
            onPress={props.onSelect}
            style={[styles.itemContainer,
            (props.selectedCategory == props.index) && styles.itemSelectedSize]}>

            <Image source={{ uri: props.item.image }} style={styles.image} />

        </SpringButton>
    </View>


)



const SIZE = 50;
const MARGIN = 5;
const SELECTED_CO = 1.3;

const styles = StyleSheet.create({
    itemSelectedSize: {
        height: SIZE * SELECTED_CO,
        width: SIZE * SELECTED_CO,
        borderRadius: (SIZE * SELECTED_CO) / 2,
    },
    container: {
        height: SIZE * SELECTED_CO,
        width: SIZE * SELECTED_CO,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: MARGIN,
    },
    itemContainer: {
        height: SIZE, width: SIZE,
        borderRadius: (SIZE) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    image: { height: SIZE - 10, width: SIZE - 10 }
});

