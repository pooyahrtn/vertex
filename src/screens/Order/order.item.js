import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, SpringButton } from '../../components';
import { OrderItem } from '../../containers/Order/order.types';
import Icon from 'react-native-vector-icons/MaterialIcons';
type Props = {
    item: OrderItem,
    count: number,
    onAddPressed: () => void,
    onSubtract: () => void
}
export default (props: Props) => {
    return (
        <View style={styles.container}>

            <SpringButton style={styles.addButtonContainer} onPress={props.onAddPressed} isOneShot={true} minimumScale={0.95}>
                {
                    props.count > 0 ?
                        <Text style={styles.countItemText}>{props.count}</Text>
                        :
                        <Icon
                            name="add"
                            color="white"
                            size={24}
                        />
                }

            </SpringButton>
            {
                props.count > 0 &&
                <SpringButton style={styles.addButtonContainer} onPress={props.onSubtract} isOneShot={true} minimumScale={0.95}>
                    <Icon
                        name="remove"
                        color="white"
                        size={24}
                    />
                </SpringButton>
            }


            <Text style={styles.priceText}>{String(props.item.price).toPersian()}</Text>
            <View style={styles.titleContainer}>
                <Text style={styles.titleText}>
                    {props.item.title}
                </Text>
                <Text >
                    {props.item.description}
                </Text>
            </View>
        </View>
    );
}

const PLUS_SIZE = 35;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        marginVertical: 8,
        marginHorizontal: 10
    },
    addButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: PLUS_SIZE,
        width: PLUS_SIZE,
        borderRadius: PLUS_SIZE / 2,
        backgroundColor: 'black',
        marginHorizontal: 5
    },
    priceText: {
        marginHorizontal: 10
    },
    titleContainer: { flex: 1, alignItems: 'flex-end' },
    titleText: { fontWeight: 'bold' },
    countItemText: { color: 'white', fontWeight: 'bold' }
});