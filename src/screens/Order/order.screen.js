import React from 'react';
import { SafeAreaView, FlatList, View, Animated, TouchableOpacity } from 'react-native';
import Image from 'react-native-fast-image';
import { OrderContainer } from '../../containers/Order';
import CategoryItem from './order.categoryItem';
import OrderItem from './order.item';
import { LoadingWrapper, SpringButton, Text } from '../../components';
import { PacmanIndicator } from 'react-native-indicators';
import styles from './order.styles';
import { screens } from '../../router';

type Props = {
    container: OrderContainer
}

export default class OrderSceen extends React.Component<Props> {
    confirmButtonAnimatedScale = new Animated.Value(0);

    componentDidMount() {
        this.props.container.initCategories();
    }
    componentDidUpdate() {
        this.resizeConfirmButtonScale();
    }

    onCategoryPressed = (index) => {
        this.props.container.selectCategory(index);
    }

    categoryItems = () => {
        if (this.props.container.state.categories.length > 0) {
            return this.props.container.state.categories[
                this.props.container.state.selectedCategoryIndex
            ].items;
        }
        return [];
    }

    resizeConfirmButtonScale = () => {
        const totalItems = this.props.container.getTotalOrderCount();

        const q = 0.5;
        const scale = ((1 - Math.pow(q, totalItems)) / (1 - q));
        Animated.spring(this.confirmButtonAnimatedScale, { toValue: scale }).start();
    }

    onConfirmButtonPressed = () => {
        this.props.navigation.navigate(screens.orderConfirm)
    }

    render() {

        return (
            <SafeAreaView style={styles.screen}>
                <LoadingWrapper
                    loading={this.props.container.state.loadingMenu}
                    loadingComponent={<PacmanIndicator color='black' />}
                >
                    <View>
                        <FlatList
                            horizontal={true}
                            inverted={true}
                            data={this.props.container.state.categories}
                            keyExtractor={(item) => item.id}
                            extraData={this.props.container.state}
                            contentContainerStyle={{ padding: 10, paddingVertical: 20 }}
                            renderItem={({ item, index }) => (
                                <CategoryItem
                                    item={item}
                                    selectedCategory={this.props.container.state.selectedCategoryIndex}
                                    index={index}
                                    onSelect={() => this.onCategoryPressed(index)}
                                />
                            )}
                        />
                    </View>

                    <View style={styles.screen}>
                        <FlatList
                            keyExtractor={(item) => item.id}
                            data={this.categoryItems()}
                            extraData={this.props.container.state}
                            renderItem={({ item }) => <OrderItem
                                item={item}
                                count={this.props.container.getItemOrderCount(item.id)}
                                onAddPressed={() => this.props.container.addOrderItem(item)}
                                onSubtract={() => this.props.container.subtractItem(item.id)}
                            />}
                            ListFooterComponent={<View style={{ height: 100, width: 10 }} />}
                        />
                    </View>
                    {
                        this.props.container.state.orders.length > 0 &&
                        <SpringButton style={styles.confirmButton} onPress={this.onConfirmButtonPressed}>
                            <Animated.View style={[styles.confirmButtonAnimatedView, { transform: [{ scale: this.confirmButtonAnimatedScale }] }]}>
                                <Text style={{ fontSize: 20, color: 'white' }}>{this.props.container.getTotalOrderCount()}</Text>
                            </Animated.View>
                        </SpringButton>

                    }



                </LoadingWrapper>
            </SafeAreaView>
        )
    }
}