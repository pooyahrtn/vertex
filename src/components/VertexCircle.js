// @flow
import React from 'react';
import { View, StyleSheet, Animated, Dimensions, TouchableWithoutFeedback } from 'react-native';
import SpringButton from './SpringButton';
import AnimationQueue from '../utils/AnimationQueue';

const { width, height } = Dimensions.get('screen');

type Props = {
    first_position?: {
        x: number,
        y: number
    },
    translateX?: number,
    translateY?: number,
    startOnRender?: boolean,
    secondRadius?: number,
    delay?: 0,
    onPress?: () => void,
    onAnimationEnded?: () => void,
    loading?: boolean,
    delay?: number,
    setTransitionRef: (ref: (y: number) => void) => void
}
export default class VertexCircle extends React.PureComponent<Props>{
    /**
     *  A circle, with animated properties. 
     * you should place it behind other views, to ensure not intercept touch properties of other views.
     * default circle position is center of the screen.
     */


    constructor(props) {
        super(props);
        this.transition_animated_value = new Animated.Value(0);
        this.scale_animated_value = new Animated.Value(1);
        this.animationQueue = new AnimationQueue();

        this.translate_x = this.transition_animated_value.interpolate({
            inputRange: [0, 1],
            outputRange: [0, (this.props.translateX || 0)]
        });


        this.target_scale = (this.props.secondRadius || DEFAULT_RADIUS) / DEFAULT_RADIUS;
        if (this.props.setTransitionRef) {
            this.props.setTransitionRef(this.transit);
        }
    }

    handleOnPressIn = () => {
        this.props.onPress();
        Animated.spring(this.scale_animated_value, {
            toValue: 0.8 * this.target_scale,
            useNativeDriver: true
        }).start();

    }

    handleOnRelease = () => {
        if (!this.props.loading) {
            Animated.spring(this.scale_animated_value, {
                toValue: this.target_scale,
                friction: this.props.friction || 3,
                tension: this.props.tension || 40,
                useNativeDriver: true
            }).start();
        }

    }

    transit = (y: number) => {

        const animated_val = new Animated.Value(0);
        const animation = Animated.timing(this.transition_animated_value, {
            toValue: y,
            duration: 400,
            useNativeDriver: true
        });
        this.animationQueue.run({ animation });
    }

    componentDidMount() {
        if (this.props.startOnRender) {
            this.animateTransition();
        }
    }

    animateTransition = () => {
        const animation = Animated.parallel([
            Animated.timing(
                this.transition_animated_value, {
                    toValue: this.props.translateY || 0,
                    duration: 400,
                    delay: this.props.delay || 0,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                this.scale_animated_value, {
                    toValue: this.target_scale,
                    duration: 400,
                    delay: this.props.delay || 0,
                    useNativeDriver: true
                }
            )
        ]);
        this.animationQueue.run({
            animation,
            onEnd: this.props.onAnimationEnded
        });
    }
    animateLoading = () => {

        this.loadingAnimation = Animated.loop(
            Animated.sequence([
                Animated.spring(this.scale_animated_value, {
                    toValue: this.target_scale * 1.2,
                    useNativeDriver: true
                }),
                Animated.spring(this.scale_animated_value, {
                    toValue: this.target_scale * 0.8,
                    useNativeDriver: true
                })
            ])
        );
        this.loadingAnimation.start();

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.loading) {
            this.animateLoading();
        } else {
            if (this.loadingAnimation) {
                this.loadingAnimation.stop();
                Animated.spring(this.scale_animated_value, { toValue: this.target_scale, useNativeDriver: true }).start();
            }
        }
    }

    render() {

        return (
            <TouchableWithoutFeedback
                onPressIn={() => this.handleOnPressIn()}
                onPressOut={this.handleOnRelease}
            >
                <Animated.View
                    style={[
                        styles.default, {

                            transform: [
                                {
                                    translateX: this.translate_x
                                }, {
                                    translateY: this.transition_animated_value
                                }, {
                                    scale: this.scale_animated_value
                                }
                            ]
                        }
                    ]}
                >
                    {
                        this.props.loading &&
                        <View style={{
                            backgroundColor: 'white',
                            width: 2 * DEFAULT_RADIUS - 4,
                            height: 2 * DEFAULT_RADIUS - 4,
                            borderRadius: DEFAULT_RADIUS - 2,

                        }} />
                    }
                </Animated.View>
            </TouchableWithoutFeedback>
        );
    }
}

const DEFAULT_RADIUS = 50;
const styles = StyleSheet.create({

    default: {
        position: 'absolute',
        top: height / 2 - DEFAULT_RADIUS,
        left: width / 2 - DEFAULT_RADIUS,
        width: DEFAULT_RADIUS * 2,
        height: DEFAULT_RADIUS * 2,
        borderRadius: DEFAULT_RADIUS,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center'
    }
})