import React from 'react';
import { TouchableWithoutFeedback, Animated, View, ViewStyle } from 'react-native';
type Props = {
    minimumScale?: number,
    friction?: number,
    tension?: number,
    style: ViewStyle,
    onPress: () => void,
    isOneShot: boolean,
};

export default class SpringButton extends React.PureComponent<Props>{
    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(1);
    }

    handleOnPressIn = () => {
        this.props.onPress();
        if (this.props.isOneShot) {
            Animated.sequence([this.shrinkAnimation(), this.enlargeAnimation()]).start();
        } else {
            this.shrinkAnimation().start();
        }

    }
    shrinkAnimation = () => {
        return Animated.spring(this.animatedValue, {
            toValue: this.props.minimumScale || 0.8,
            useNativeDriver: true
        });
    }


    enlargeAnimation = () => (
        Animated.spring(this.animatedValue, {
            toValue: 1,
            friction: this.props.friction || 3,
            tension: this.props.tension || 40,
            useNativeDriver: true
        })
    )

    handleOnRelease = () => {
        if (!this.props.isOneShot) {
            this.enlargeAnimation().start();

        }
    }

    render() {
        const animatedStyle = {
            transform: [{ scale: this.animatedValue }]
        }
        return (

            <Animated.View
                onTouchStart={this.handleOnPressIn}
                onTouchEnd={this.handleOnRelease}
                style={[animatedStyle, this.props.style]}>
                {this.props.children}
            </Animated.View>

        );
    }
}

