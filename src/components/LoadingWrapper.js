import React from 'react';
import { View } from 'react-native';
type Props = {
    loading: boolean,
    loadingComponent: any
}
export default function (props: Props) {
    if (props.loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                {props.loadingComponent}
            </View>
        );
    }
    return (
        <View style={{ flex: 1 }}>
            {props.children}
        </View>
    );

}
