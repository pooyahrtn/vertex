import React from 'react';
import { Animated } from 'react-native';
import { Path, Svg } from 'react-native-svg';

type Props = {
    radius: number,
    padding: number,
    animationProgress: number,
    value: number,
    rotation: number,
    containerStyle: object,
    enableRotation: boolean
}

function progressToRadian(progress) {
    return (2 * Math.PI * progress);
}

class Circle extends React.Component<Props> {
    setNativeProps = (props) => {
        this._component && this._component.setNativeProps(props);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.animationProgress !== this.props.animationProgress;
    }

    render() {
        let { padding, width, value, animationProgress } = this.props;

        let radius = width / 2 - padding;

        let start_degree = progressToRadian(animationProgress * (1 - value));
        let end_degree = start_degree + progressToRadian(animationProgress * value)

        let start_point_left = padding + radius + radius * Math.sin(start_degree);
        let start_point_top = padding + radius - radius * Math.cos(start_degree);

        let end_point_left = padding + radius + radius * Math.sin(end_degree);
        let end_point_top = padding + radius - radius * Math.cos(end_degree);
        let half_circle = '';

        if ((end_degree - start_degree) > Math.PI) {
            half_circle = `A ${radius} ${radius} 0 0 1 ${radius + padding} ${padding + 2 * radius}`
        };

        return (
            <Svg
                ref={component => (this._component = component)}

                style={[this.props.containerStyle,
                { width: 2 * (padding + radius), height: 2 * (padding + radius) }]}
                height={2 * (padding + radius)}
                width={2 * (padding + radius)}
            >
                <Path
                    // strokeLinecap='round'
                    strokeWidth={5}
                    stroke="black"
                    fill="none"
                    d={`
                        M ${start_point_left} ${start_point_top} 
                        ${half_circle}
                        A ${radius} ${radius} 0 0 1 ${end_point_left} ${end_point_top}
                    `}
                />
            </Svg>

        );
    }
}


class CircleWrapper extends React.Component {
    // setNativeProps = (props) => {
    //     this._component && this._component.setNativeProps(props);
    // }

    render() {
        return (
            <Circle
                ref={component => (this._component = component)}
                {...this.props}
            />
        );
    }
}

export default Animated.createAnimatedComponent(CircleWrapper);