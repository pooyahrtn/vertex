import React from 'react';
import { Text, TextStyle } from 'react-native';

type Props = {
    style: TextStyle
}
function CText(props: Props) {
    return <Text style={[{ direction: 'rtl', textAlign: 'right', color: 'black' }, props.style]}>{props.children}</Text>
}
export default CText;