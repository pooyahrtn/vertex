import React from 'react';
import { Animated } from 'react-native';
import { Line, LineProps } from 'react-native-svg';

class LineWrapper extends React.Component<LineProps> {
    setNativeProps = (props) => {
        this._component && this._component.setNativeProps(props);
    }

    shouldComponentUpdate(nextProps: LineProps) {
        return (nextProps.x2 !== this.props.x2 || nextProps.x1 !== this.props.x1);
    }

    render() {
        return (
            <Line
                ref={component => (this._component = component)}
                {...this.props}
            />
        );
    }
}

export default Animated.createAnimatedComponent(LineWrapper);