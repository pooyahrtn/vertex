// @flow
import React from 'react';
import { Container, Subscribe } from 'unstated';

export default function (Component, extraProps = {}, ...store) {
    return function Wrapper(props) {
        return (
            <Subscribe to={store}>
                {container => <Component container={container} {...props} {...extraProps} />}
            </Subscribe>
        );
    };
}