export type MenuItem = {
    id: String,
    title: String,
    description: String,
    price: number,
}

export type MenuCategory = {
    id: String,
    image: String,
    items: [MenuItem]
}

export type OrderItem = {
    count: number,
    item: MenuItem
}