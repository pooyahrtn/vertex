// @flow
import { Container } from 'unstated';
import { MenuItem, MenuCategory, OrderItem } from './order.types';

type StateType = {
    categories: [MenuCategory],
    loadingMenu: boolean,
    orders: [OrderItem],
    selectedCategoryIndex: number,
}

export default class OrderConainer extends Container {
    state: StateType = {
        categories: [],
        loadingMenu: false,
        orders: [],
        selectedCategoryIndex: 0,
    }

    initCategories = () => {
        this.setState({
            loadingMenu: true,
        });
        setTimeout(() => {
            this.setState({
                categories,
                loadingMenu: false
            });
        }, 1500);

    }

    selectCategory = (index) => {
        this.setState({
            selectedCategoryIndex: index
        });
    }

    addOrderItem = (item) => {
        let currentOrders = [...this.state.orders];
        let itemInOrder = currentOrders.find((v) => v.item.id === item.id);
        if (itemInOrder) {
            itemInOrder.count += 1;
        } else {
            currentOrders.push({
                count: 1,
                item
            })
        };
        this.setState({
            orders: currentOrders
        });
    }

    getItemOrderCount = (id) => {
        const itemInOrder = this.state.orders.find((item) => item.item.id === id);

        if (itemInOrder) {
            return itemInOrder.count;
        }
        return 0;
    }

    getTotalOrderCount = () => {
        const orders = this.state.orders;
        const itemCounts = orders.map(item => item.count);
        const totalItems = itemCounts.reduce((acc, cur) => acc + cur, 0);
        return totalItems;
    }

    subtractItem = (id) => {
        let currentOrders = [...this.state.orders];
        let itemInOrder = currentOrders.find((item) => item.item.id === id);
        if (itemInOrder.count > 1) {
            itemInOrder.count -= 1;
        } else {
            const itemIndex = currentOrders.findIndex((item) => item.id === id);
            currentOrders.splice(itemIndex, 1);
        };
        this.setState({
            orders: currentOrders
        });
    }

    getTotalPriceSum = () => {
        const round = (val) => Math.round(val * 100) / 100;
        return this.state.orders.reduce((acc, cur) => round(acc + cur.count * cur.item.price), 0);
    }


}




export const categories: [MenuCategory] = [
    {
        id: '1',
        image: 'https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/dining-w-icon.png',
        items: [
            {
                id: '1',
                title: 'چای ارگانیک لاهیجان با کوکی',
                price: 9.9,
                description: 'چایی طبیعی از لاهیجان'
            },
            {
                id: '2',
                title: 'چای زعفران و بهارنارنج',
                price: 10.2,
                description: 'بدون کوکی'
            },
            {
                id: '3',
                title: 'چای بهارنارنج و بهارنارنج',
                price: 10.2,
                description: 'بدون کوکی'
            },
            {
                id: '4',
                title: 'چای سبز و بهارنارنج',
                price: 10.2,
                description: 'بدون کوکی'
            },
            {
                id: '5',
                title: 'چای زرد و بهارنارنج',
                price: 10.2,
                description: 'بدون کوکی'
            },
        ]
    },
    {
        id: '2',
        image: 'https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/dining-w-icon.png',
        items: [
            {
                id: '1x',
                title: 'استیک ارگانیک لاهیجان با کوکی',
                price: 9.9,
                description: 'چایی طبیعی از لاهیجان'
            },
            {
                id: '2x',
                title: 'برگر زعفران و بهارنارنج',
                price: 10.2,
                description: 'بدون کوکی'
            },
            {
                id: '3x',
                title: 'پاستا بهارنارنج و بهارنارنج',
                price: 19.2,
                description: 'بدون کوکی'
            },
            {
                id: '4x',
                title: 'پنینی سبز و بهارنارنج',
                price: 30.2,
                description: 'بدون کوکی'
            },
            {
                id: '5x',
                title: 'پنکیک زرد و بهارنارنج',
                price: 80.2,
                description: 'بدون کوکی'
            },
        ]
    }
]

