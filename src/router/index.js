import { createAppContainer } from 'react-navigation';
import screenStack from './screenStack';

export { screens } from './names';

export default createAppContainer(
    screenStack
);


