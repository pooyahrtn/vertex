import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { screens } from './names';
import Home from '../screens/Home';
import Order from '../screens/Order';
import Header from './Header';
import OrderConfirm from '../screens/OrderConfirm';

export default createStackNavigator({
    [screens.home]: {
        screen: Home
    },
    [screens.order]: {
        screen: Order
    },
    [screens.orderConfirm]:{
        screen: OrderConfirm
    }
}, {
        defaultNavigationOptions: {
            header: (props) => <Header {...props} />
        }
    });