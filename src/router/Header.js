import React from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import { HeaderBackButton, HeaderProps, SafeAreaView, } from 'react-navigation';
import { Text } from '../components';

export default class Header extends React.PureComponent<HeaderProps> {
    render() {
        // console.warn(this.props.navigation.goBack)
        return (
            <SafeAreaView style={{ backgroundColor: 'black' }}>

                <View style={{ flexDirection: 'row', height: 40 }}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>VERTEX</Text>
                    </View>
                    {
                        (Platform.OS === 'ios' && this.props.index !== 0) &&
                        <HeaderBackButton
                            onPress={() => {

                                this.props.navigation.goBack(null)
                            }}
                            tintColor="#fff"
                        />
                    }

                </View>

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    title: {
        color: 'white',
        fontSize: 20,
    },
    titleContainer: {
        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
});